# Demo run of NoiseChisel and Segment on M51.
#
# Copyright (C) 2019-2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.





# Pixel scale of input image
# --------------------------
#
# In order to estimate the surface brightness level in units of magnitude
# per arcseconds, it is necessary to know the pixel scale of the image in
# units of arcsec^2 (which is independent of every individual run).
demopixscale = $(indir)/pixel-scale.txt
$(demopixscale): $(indir)/sdss-r-m51.fits
	astfits $< -h1 | awk '/CDELT1/ {p=$$3*3600; print p*p}' > $@





# Default NoiseChisel
# -------------------
#
# Run NoiseChisel with the default parameters.
ncdir=$(badir)/noisechisel
demoncdefault = $(ncdir)/r-default.fits
$(ncdir):; mkdir $@
$(demoncdefault): $(ncdir)/%-default.fits: $(indir)/sdss-%-m51.fits | $(ncdir)
	astnoisechisel $< -o$@





# Optimized NoiseChisel
# ---------------------
#
# Run NoiseChisel with parameters optimized for the noise properties of
# this image.
demoncoptimized = $(ncdir)/r-optimized.fits
$(demoncoptimized): $(ncdir)/%-optimized.fits: $(indir)/sdss-%-m51.fits \
                    $(pconfdir)/noisechisel-options.conf | $(ncdir)
	astnoisechisel $< -o$@ \
	               --snquant=$(noisechisel-snquant) \
	               --tilesize=$(noisechisel-tilesize) \
	               --minskyfrac=$(noisechisel-minskyfrac) \
	               --meanmedqdiff=$(noisechisel-meanmedqdiff) \
	               --interpnumngb=$(noisechisel-interpnumngb) \
	               --detgrowquant=$(noisechisel-detgrowquant) \
	               --detgrowmaxholesize=$(noisechisel-detgrowmaxholesize)





# NoiseChisel border pixels
# -------------------------
#
# In order to estimate the surface brightness level reached, we'll identify
# all the border pixels.
borderdir=$(badir)/border
demoedgepixs = $(foreach m, default optimized, $(borderdir)/r-$(m).fits)
$(borderdir):; mkdir $@
$(demoedgepixs): $(borderdir)/r-%.fits: $(ncdir)/r-%.fits | $(borderdir)

#	Give a label to each detected region, but first erode two layers
#	to remove the thin correlated noise.
	lab=$(borderdir)/r-$*-lab.fits
	astarithmetic $< -hDETECTIONS 2 erode 2 erode \
	              2 connected-components -o$$lab

#	Find the largest label.
	cat=$(borderdir)/r-$*-cat.txt
	astmkcatalog $$lab --ids --geoarea -h1 -o$$cat
	id=$$(awk '!/^#/{if($$2>max) {id=$$1; max=$$2}} END{print id}' $$cat)
	echo $$id > $(borderdir)/r-$*-id.txt

#	Find the border pixels using erosion.
	astarithmetic $$lab $$id eq set-i i \
	              i 2 erode 2 erode 2 erode 0 where -o$@

#	Clean up.
	rm $$lab $$cat





# Default SExtractor
# ------------------
sedir=$(badir)/sextractor
m51se = $(sedir)/r-se.fits
$(sedir):; mkdir $@
$(m51se): $(sedir)/%-se.fits: $(indir)/sdss-%-m51.fits \
          $(pconfdir)/sextractor.conf \
          $(seconfdir)/sextractor.config | $(sedir)
	sextractor -c $(seconfdir)/sextractor.config  \
	    $(indir)/sdss-$*-m51.fits \
	    -CATALOG_NAME $@ \
	    -CATALOG_TYPE FITS_1.0 \
	    -DETECT_THRESH $(sextractor-detect-thresh) \
	    -ANALYSIS_THRESH $(sextractor-analysis-thresh) \
	    -PARAMETERS_NAME $(seconfdir)/columns.conf \
	    -FILTER_NAME $(seconfdir)/default.conv \
	    -CHECKIMAGE_TYPE "SEGMENTATION,APERTURES" \
	    -CHECKIMAGE_NAME "$(sedir)/seg.fits,$(sedir)/aper.fits"





# SExtractor border pixels
# ------------------------
seedge = $(borderdir)/se.fits
$(seedge): $(sedir)/r-se.fits | $(borderdir)
#	Only keep the main object (regions larger than 1000 pixels and in
#	the upper-half of the image (to ignore the star in the bottom).
	labs=$$(asttable $< -cNUMBER \
	                 --range=ISOAREAF_IMAGE,1000,inf \
	                 --range=Y_IMAGE,500,inf \
	                 | awk 'NR==1{printf "seg %d eq ",    $$1} \
	                        NR>1 {printf "seg %d eq or ", $$1}')

#	Estimate the edges. We are dilating the original detection once
	astarithmetic $(sedir)/seg.fits -h0 set-seg $$labs \
	              2 dilate 2 dilate set-i i \
	              i 2 erode 2 erode 2 erode 0 where -o$@




# Border Surface brightness
# -------------------------
#
# Note that we need to use the same noise-level (the optimized one) for
# both, otherwise we won't be able to compare them. So for both cases,
# we'll need the optimized NoiseChisel result.
bordersf = $(foreach m, default optimized, $(borderdir)/r-$(m)-sf.txt)
$(bordersf): $(borderdir)/r-%-sf.txt: $(borderdir)/r-%.fits \
             $(demoncoptimized) $(demopixscale)

#	In units of S/N.
	skystd="$(demoncoptimized) -hSKY_STD"
	skysub="$(demoncoptimized) -hINPUT-NO-SKY"
	sn=$$(astarithmetic $$skysub $$skystd / $(borderdir)/r-$*.fits \
	                      not nan where meanvalue --quiet)

#	In units of magnitudes/arcsec^2.
	pixscale=$$(cat $(demopixscale))
	mean=$$(astarithmetic $$skysub $(borderdir)/r-$*.fits not nan where \
	                      meanvalue -q)
	sf=$$(echo "$$mean $$pixscale $(zeropoint-nanomaggie)" \
	           | awk '{print -2.5*log($$1/$$2)/log(10)+$$3}')

#	Write the two numbers in the target text file.
	echo "$$sn $$sf" > $@





# Demo PDF image
# --------------
demodir = $(texdir)/m51
demodetection = $(demodir)/detection.pdf
$(demodir):; mkdir $@
$(demodetection): $(demoedgepixs) $(seedge) | $(demodir)

#	Make sure TiKZ rebuilds the figure.
	rm -f $(tikzdir)/detection*

#	For easy reading.
	colorsn=2.5
	logmin=-1
	fluxlow=-1
	fluxhigh=3
	maxinhigh=50
	sn=$(demodir)/sn.fits
	red=$(demodir)/red.fits
	base=$(demodir)/base.fits
	blue=$(demodir)/blue.fits
	green=$(demodir)/green.fits

#	The S/N image for easy definition of colors.
	skystd="$(demoncoptimized) -hSKY_STD"
	skysub="$(demoncoptimized) -hINPUT-NO-SKY"
	astarithmetic $$skysub $$skystd / -o$$sn

#	Scale the high-S/N region to fit the `flux-high' that we will use
#	in the final conversion.
	astarithmetic $$sn set-sn \
	              sn $$fluxhigh gt 2 erode 2 dilate 2 dilate set-bin \
	              sn bin not 0 where set-highsn \
	              highsn highsn $$maxinhigh gt $$maxinhigh where \
	              $$maxinhigh / $$fluxhigh x set-in \
	              sn in 0 ne nan where set-out \
	              out $$logmin - log set-outlog \
	              outlog outlog isblank 0 where set-outraw \
	              outraw outraw maxvalue / $$fluxhigh x set-out \
	              out in + -o$$base

#	Make the different color channels.
	astarithmetic $(borderdir)/se.fits set-lab \
	              $$base lab 0 where lab $$colorsn x + -g1 -o$$red
	astarithmetic $(borderdir)/r-default.fits set-lab \
	              $$base lab 0 where lab $$colorsn x + -g1 -o$$blue
	astarithmetic $(borderdir)/r-optimized.fits set-lab \
	              $$base lab 0 where lab $$colorsn x + \
	              $(borderdir)/r-default.fits set-lab  \
	              lab 0 where lab $$colorsn x + -g1 -o$$green

#	Make the final PDF.
	astconvertt $$red $$green $$blue -h1 -h1 -h1 -o$@ \
	            --borderwidth=1 --width=20 --fluxlow=$$fluxlow

#	Clean up
	rm $$red $$green $$blue $$base $$sn





# M51 Sky level
# -------------
#
# The Sky-level will show that there is still undetected signal beyond the
# optimized NoiseChisel result.
m51sky = $(demodir)/sky.pdf
$(m51sky): $(ncdir)/r-optimized.fits
	astconvertt $< -hSKY --fluxhigh=0.00093 -o$@





# Segmentation
# ------------
segdir = $(badir)/segmentation
m51seg = $(segdir)/r.fits
$(segdir):; mkdir $@
$(m51seg): $(segdir)/%.fits: $(ncdir)/%-optimized.fits | $(segdir)

#	Run Segment.
	seg=$(subst .fits,_seg.fits,$@)
	astsegment $< -o$$seg

#	Mask the clumps.
	astarithmetic $$seg -h1 $$seg -hCLUMPS 0 gt nan where -o$@





# Segmentation PDF
# ----------------
segpdf = $(demodir)/seg-clumps.pdf
$(segpdf): $(segdir)/r.fits
#	The same high flux value.
	fh=1.5
	sec=:300,780:1100

#	Make sure TiKZ rebuilds the figure.
	rm -f $(tikzdir)/segmentation*

#	Input crop
	segcrop=$(demodir)/seg-crop.fits
	astcrop $(segdir)/r_seg.fits -h1 --mode=img --section=$$sec \
	        -o$$segcrop
	astconvertt $$segcrop --fluxhigh=$$fh -o$(demodir)/seg-input.pdf

#	Clumps masked.
	astcrop $(segdir)/r.fits -h1 --mode=img --section=$$sec \
	        -o$$segcrop
	astconvertt $$segcrop --fluxhigh=$$fh -o$(demodir)/seg-masked.pdf

#	Raw clumps
	astcrop $(segdir)/r_seg.fits -h2 --mode=img --section=$$sec \
	        -o$$segcrop
	astconvertt $$segcrop --fluxhigh=1 --invert -o$@

#	Clean up.
	rm $$segcrop





# Final TeX macro
# ---------------
$(mtexdir)/m51.tex: $(bordersf) $(demodetection) $(segpdf) $(m51sky) \
                    | $(mtexdir)

#	Version of Gnuastro and SExtractor
	v=$$(astnoisechisel --version | awk 'NR==1{print $$NF}')
	echo "\newcommand{\gnuastroversion}{$$v}"                > $@
	v=$$(sextractor --version | awk 'NR==1{print $$3}')
	echo "\newcommand{\sextractorversion}{$$v}"             >> $@

#	Report the surface brightness values.
	v=$$(awk '{printf "%.2f", $$1}' $(borderdir)/r-default-sf.txt)
	echo "\newcommand{\demosfdefaultsn}{$$v}"               >> $@
	v=$$(awk '{printf "%.2f", $$2}' $(borderdir)/r-default-sf.txt)
	echo "\newcommand{\demosfdefaultmagperarc}{$$v}"        >> $@
	v=$$(awk '{printf "%.2f", $$1}' $(borderdir)/r-optimized-sf.txt)
	echo "\newcommand{\demosfoptimizedsn}{$$v}"             >> $@
	v=$$(awk '{printf "%.2f", $$2}' $(borderdir)/r-optimized-sf.txt)
	echo "\newcommand{\demosfoptimizedmagperarc}{$$v}"      >> $@

#	Zeropoints.
	zpdiff=$$(echo $(zeropoint-asinh-r) $(zeropoint-nanomaggie) \
	               | awk '{print $$1 - $$2}')
	echo "\newcommand{\sdsszpdiff}{$$zpdiff}" >> $@
	echo "\newcommand{\sdsszpasinhr}{$(zeropoint-asinh-r)}" >> $@
	echo "\newcommand{\sdsszpnanomaggie}{$(zeropoint-nanomaggie)}" >> $@

#	NoiseChisel parameters
	echo "\newcommand{\ncsnquant}{$(noisechisel-snquant)}" >> $@
	echo "\newcommand{\nctilesize}{$(noisechisel-tilesize)}" >> $@
	echo "\newcommand{\ncminskyfrac}{$(noisechisel-minskyfrac)}" >> $@
	echo "\newcommand{\ncinterpnumngb}{$(noisechisel-interpnumngb)}" >> $@
	echo "\newcommand{\ncdetgrowquant}{$(noisechisel-detgrowquant)}" >> $@
	echo "\newcommand{\ncmeanmedqdiff}{$(noisechisel-meanmedqdiff)}" >> $@
	echo "\newcommand{\ncdetgrowmaxholesize}{$(noisechisel-detgrowmaxholesize)}" >> $@

#	SExtractor parameters
	echo "\newcommand{\sedetectthresh}{$(sextractor-detect-thresh)}" >> $@
	echo "\newcommand{\seanalysisthresh}{$(sextractor-analysis-thresh)}" >> $@
