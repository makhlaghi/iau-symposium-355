# Examples of undetected objects in UVUDF.
#
# Copyright (C) 2019-2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.





# MUSE IDs
museids = $(uvudf-rd1-museid) $(uvudf-rd2-museid) \
          $(uvudf-rc1-museid) $(uvudf-rc2-museid) \
          $(uvudf-rs1-museid) $(uvudf-rs2-museid)





# Make cutouts of the selected objects
uvudfdir = $(texdir)/uvudf
uvudfcutouts = $(foreach i, $(museids), $(uvudfdir)/$(i).pdf)
$(uvudfdir):; mkdir $@
$(uvudfcutouts): $(uvudfdir)/%.pdf: $(indir)/xdf-f775w.fits \
                 $(pconfdir)/uvudf-missing.conf \
                 $(indir)/uvudf-seg.fits.gz \
                 | $(uvudfdir)

#	Delete the TiKZ image so it is rebuilt.
	rm -f $(tikzdir)/uvudf*

#	Set the RA and Dec for each object.
	if   [ $* = $(uvudf-rd1-museid) ]; then radec=$(uvudf-rd1-radec); \
	elif [ $* = $(uvudf-rd2-museid) ]; then radec=$(uvudf-rd2-radec); \
	elif [ $* = $(uvudf-rc1-museid) ]; then radec=$(uvudf-rc1-radec); \
	elif [ $* = $(uvudf-rc2-museid) ]; then radec=$(uvudf-rc2-radec); \
	elif [ $* = $(uvudf-rs1-museid) ]; then radec=$(uvudf-rs1-radec); \
	elif [ $* = $(uvudf-rs2-museid) ]; then radec=$(uvudf-rs2-radec); \
	else echo "ID $* not recognized"; \
	fi

#	Cutout of segmentation image.
	tmp=$(uvudfdir)/$*.fits
	cropopts="--mode=wcs --center=$$radec --width=$(uvudf-w-arcsec)/3600"
	astcrop $(indir)/uvudf-seg.fits.gz -h0 $$cropopts -o$$tmp
	astconvertt $$tmp --fluxhigh=1 --invert -o$(uvudfdir)/$*-seg.pdf
	mv $$tmp $(uvudfdir)/$*-seg.fits

#	Cutout of F775W image.
	astcrop $(indir)/xdf-f775w.fits -h0 $$cropopts -o$$tmp
	astconvertt $$tmp --fluxhigh=0.003 -o$@

#	Clean up.
	rm $$tmp





# Final TeX macro
# ---------------
$(mtexdir)/uvudf.tex: $(uvudfcutouts) | $(mtexdir)

#	Keep the IDs to report in the paper.
	echo "\newcommand{\uvudfrdoneid}{$(uvudf-rd1-museid)}"  > $@
	echo "\newcommand{\uvudfrdtwoid}{$(uvudf-rd2-museid)}" >> $@
	echo "\newcommand{\uvudfrconeid}{$(uvudf-rc1-museid)}" >> $@
	echo "\newcommand{\uvudfrctwoid}{$(uvudf-rc2-museid)}" >> $@
	echo "\newcommand{\uvudfrsoneid}{$(uvudf-rs1-museid)}" >> $@
	echo "\newcommand{\uvudfrstwoid}{$(uvudf-rs2-museid)}" >> $@
