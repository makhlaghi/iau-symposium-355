# Demonstrate the effect of NoiseChisel's `--detgrowquant'.
#
# Copyright (C) 2019-2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.





# Unpack input image
# ------------------
#
# The raw SDSS input image doesn't have standard WCS. We'll thus do a
# redundant arithmetic operation on it, which will automatically fix the
# WCS.
inputunpacked = $(foreach f, g r i, $(indir)/sdss-$(f)-m51.fits)
$(inputunpacked): $(indir)/%.fits: $(indir)/%.fits.bz2

#	Unpack the input into a temporary file (`$@.fits').
	bunzip2 -k -c $< > $@.fits

#	Run Arithmetic on it to correct its WCS, then clean up.
	astarithmetic $@.fits -h0 -o$@
	rm $@.fits





# Crop smaller part for demonstration
# -----------------------------------
dgqdir=$(badir)/detgrowquant
dgqcropped = $(dgqdir)/cropped.fits
$(dgqdir):; mkdir $@
$(dgqcropped): $(indir)/sdss-r-m51.fits | $(dgqdir)
	astcrop $< --mode=img --section=1500:,1: -o$@





# Various NoiseChisel runs
dgqdemodir = $(texdir)/detgrowquant
dgqquantiles = 1.00 0.99 0.95 0.90 0.80 0.75 0.70 0.65 0.60
dgqnc = $(foreach q, $(dgqquantiles), $(dgqdir)/$(q).fits)
$(dgqnc): $(dgqdir)/%.fits: $(dgqcropped)
	rm -f $(tikzdir)/detgrowquant*
	astnoisechisel $< --detgrowquant=$* --detgrowmaxholesize=200 -o$@





# Demonstration region
# --------------------
dgqdemocrop = --mode=img --section=322:,905:1108
dgqdemoin = $(dgqdemodir)/in.pdf
dgqdemos = $(foreach q, $(dgqquantiles), $(dgqdemodir)/$(q).txt)
$(dgqdemodir):; mkdir $@
$(dgqdemoin): $(dgqcropped) | $(dgqdemodir)
	rm -f $(tikzdir)/detgrowquant*
	crop=$(dgqdemodir)/in.fits
	astcrop $< $(dgqdemocrop) -o$$crop
	astconvertt --fluxhigh=0.1 $$crop -o$@
	rm $$crop

$(dgqdemos): $(dgqdemodir)/%.txt: $(dgqdir)/%.fits | $(dgqdemodir)
#	Make sure TiKZ builds the figure again.
	rm -f $(tikzdir)/detgrowquant*

#	Do the crop.
	crop=$(dgqdemodir)/$*.fits
	astcrop $< -hDETECTIONS $(dgqdemocrop) -o$$crop

#	Unfortunately LaTeX won't work with file names like 0.99.pdf:
#	because it won't recognize the `99.pdf' suffix!!! So we'll have
#	to remove the `.'.
	astconvertt $$crop --invert -o$(dgqdemodir)/$(subst .,,$*).pdf

#	Build target and clean up.
	touch $@
	rm $$crop





# Final TeX macro
# ---------------
$(mtexdir)/detgrowquant.tex: $(dgqdemos) $(dgqdemoin) | $(mtexdir)
	echo "% no macros for detgrowquant.mk" > $@
